FROM ubuntu:latest
RUN apt-get update && apt-get -y install python3 && apt-get -y install python3-pip
RUN pip3 install scipy
RUN pip3 install kafka-python
COPY ./ /opt/vb
ENV PYTHONUNBUFFERED=1
CMD ["python3", "/opt/vb/analytics.py"]
