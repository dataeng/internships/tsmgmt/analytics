# Import KafkaConsumer from Kafka library
from kafka import KafkaConsumer
from kafka import KafkaProducer
from json import loads
from json import dumps
from multiprocessing import Process
import sys, datetime, time

# Define server with port
bootstrap_servers = ['kafka.default.svc.cluster.local:9092']
#bootstrap_servers = ['localhost:9092'] #(Switch to this for local runs)

#Used to measure PH and Temperature.
def consumeData(topic):
    try:
        consumer = KafkaConsumer(topic,bootstrap_servers= bootstrap_servers,
        value_deserializer=lambda x: loads(x.decode('utf-8')))
    except:
        print("Error in Consumer creation!")

    #The Producer object will be used in order to send data to Timescale DB.
    producer_server = ['kafka-0.kafka-headless.default.svc.cluster.local:9092']    
    producer = KafkaProducer(bootstrap_servers = producer_server,value_serializer=lambda x: dumps(x).encode('utf-8'))

    #Initializing the values that may be needed for the sum calculation.
    ph_timestamp = 0
    ph_val = 0
    temp_timestamp = 0
    temp_val = 0
    message_queue = []

    #Receiving the messages  
    for msg in consumer: 
        #print(msg)
        
        json_data = msg.value

        #Checking if we have ph or temperature.
        if json_data['topic']=='ph':
            if message_queue != []:
                if message_queue[0].value['topic'] == 'ph': #Remove a ph value if it's already in the queue.
                    message_queue.pop(0)
            message_queue.append(msg)
            print('Ph received from IoT_2: ', json_data['ph'])
        elif json_data['topic']=='temp':
            print('Temperature received from IoT_1: ', json_data['temperature'], ' celsius')
            if float(json_data['temperature']) > 4.0 :
                if message_queue != []:
                    if message_queue[0].value['topic'] == 'temp': #Remove a temp value if it's already in the queue.
                        message_queue.pop(0)
                message_queue.append(msg)

        if len(message_queue) == 2: #Since we have two messages in the queue we must check if we will calculate their sum and remove them both, or the oldest entry.
            
            #print(message_queue)

            #Setting our timestamp and temp/ph values.
            if message_queue[0].value['topic'] == 'temp':
                temp_timestamp = message_queue[0].timestamp
                temp_val = float(message_queue[0].value['temperature'])
                
                ph_timestamp = message_queue[1].timestamp
                ph_val = float(message_queue[1].value['ph'])
            else:
                temp_timestamp = message_queue[1].timestamp
                temp_val = float(message_queue[1].value['temperature'])
                
                ph_timestamp = message_queue[0].timestamp
                ph_val = float(message_queue[0].value['ph'])

            #Getting date format.    
            ph_date = datetime.datetime.fromtimestamp(ph_timestamp // 1000)
            temp_date = datetime.datetime.fromtimestamp(temp_timestamp // 1000)
            
            #Getting their difference.
            diff = 0
            if ph_date > temp_date:
                diff = (ph_date-temp_date).total_seconds()
            else:
                diff = (temp_date-ph_date).total_seconds()


            #If the difference is within 2.5 seconds print the sum and remove the values from the queue.
            if diff <= 2.5:
                sum_up = float(temp_val) + float(ph_val)
                sum_up = round(sum_up, 2)
                
                #Sending results to Timescale.
                analytics_set_db = {"schema": {"type": "struct", "optional": False, "fields": [{"field": "calculation", "type": "float", "optional": False }, {"field": "temperature", "type": "float", "optional": False }, {"field": "ph", "type": "float", "optional": False }]},"payload": {"calculation": float(sum_up), "temperature": float(temp_val), "ph": float(ph_val)}}
                producer.send('analytics_01', analytics_set_db)
                
                print('The Sum of Temperature: ',temp_val,' and Ph:',ph_val,' is: ', sum_up)
                print('Time Difference: ',diff)
                message_queue.pop(0)
                message_queue.pop(0)
            else:#Otherwise remove the oldest entry in the queue. The newer entry will remain and wait for the next values.
                message_queue.pop(0)
                    

#Used to measure Open Door checks 
def consumeDoorCheck(topic, interval):
    try:
        consumer = KafkaConsumer(topic,bootstrap_servers= bootstrap_servers,
        value_deserializer=lambda x: loads(x.decode('utf-8')))
    except:
        print("Error in Consumer creation!")

    while True:
          
        #Consumer must start polling after at least 21 sec.
        messages = consumer.poll(timeout_ms=interval*21000)        
        if messages != {}:

            #Getting all door_check values from the sleeping time.
            door_checks = []
            for tp, message in messages.items():
                for m in message:
                    door_checks.append(m)
                    
            #Printing the latest value.
            latest_door = door_checks[-1]                
            print('Open door check from IoT_1: ', latest_door.value['open_door'])

        time.sleep(interval)  

        
#Using different process for each consumer.
t1 = Process(target=consumeData, args=('sample2',))
t2 = Process(target=consumeDoorCheck, args=('sample3',21))


t1.start()
t2.start()
