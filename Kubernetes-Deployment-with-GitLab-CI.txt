In this file I will explain how to deploy a k8s pod using GitLab's CI/CD pipeline. Make sure you have a Kubernetes cluster already deployed before you continue. You may also check the file: Kubernetes-Kafka-Installation.txt

In order to deploy something to our k8s cluster with GitLab we'll need to add the cluster to our GitLab project. The official documentation covers everything we need about this step: https://docs.gitlab.com/ee/user/project/clusters/add_existing_cluster.html

Also to deploy images using GitLab's pipeline system we'll have to add two CI/CD variables: KUBE_URL and KUBE_TOKEN. The first one is the URL of the k8s API server. The second one is a token and it can be obtained using the link provided above.

Now let's start creating an image to deploy. We will deploy a python script in our k8s cluster.

Let's write a simple python script that outputs 'Hello from Python' every 5 seconds:

import time

while True:
    time.sleep(5)
    print("Hello from Python!")


if __name__ == "__main__":
     main()

Let's name this file as main.py.

Now we'll dockerize the python script. Create the following Dockerfile:
FROM ubuntu:latest
RUN apt-get update && apt-get -y install python3
COPY ./ /opt/vb
ENV PYTHONUNBUFFERED=1
CMD ["python3", "/opt/vb/main.py"]

We're also going to need a deployment file for k8s. Let's create a k8s.yaml deployment file:
apiVersion: v1
kind: Namespace
metadata:
  name: default
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hello-world-deployment-1
  namespace: default
spec:
  replicas: 1
  selector:
    matchLabels:
      app: hello-1
  template:
    metadata:
      namespace: default
      labels:
        app: hello-1
    spec:
      containers:
        - name: hello-world-1
          image: registry.gitlab.com/pantelis-test-group/init-project:v2
          imagePullPolicy: Always
      dnsPolicy: ClusterFirst
      imagePullSecrets:
        - name: regcred

As you can see, to pull from GitLab's container registry, k8s needs a secret. You can create a secret (named regcred, as noted in k8s.yaml) using: kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com --docker-username=<username> --docker-password=<password>

Finally, the last file we'll need is .gitlab-ci.yml. We will give our pipeline two stages. A 'build' stage to build the image and a 'deploy' stage to deploy it in k8s. Ideally you should also use a 'test' stage, being placed between the two stages noted above. We'll choose to skip this for now. So, our .gitlab-ci.yml file is:
stages:
  - build
  - deploy

build:
  stage: build
  image: docker:latest
  services: 
    - docker:dind
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:v2
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull -t "$IMAGE_TAG" .
    - docker push "$IMAGE_TAG"
  
deploy:
  stage: deploy
  image: roffe/kubectl
  script:
    - kubectl config set-cluster cluster-first --server="$KUBE_URL" --insecure-skip-tls-verify=true
    - kubectl config set-credentials pantmal --token="$KUBE_TOKEN"
    - kubectl config set-context default --cluster=cluster-first --user=pantmal
    - kubectl config use-context default
    - sed -i "s/__VERSION__/gitlab-$CI_COMMIT_SHORT_SHA/" k8s.yaml
    - kubectl apply -f k8s.yaml

After we commit the .gitlab-ci.yml file, the pipeline in GitLab should begin. We can see its progress in the CI/CD->Pipelines section of our project. 

After a while, and if the pipeline is correct, we should see a green checkmark for both stages. If this is the case, we can now connect to k8s and see our image outputing the 'Hello from Python' string.

First get the pod's name using: kubectl get pods

Copy the name of the pod you deployed and now run the command: kubectl logs -f <your_pod_name>

You should see the 'Hello from Python' string printed every five seconds.

For our simulated devices, the Python scripts acting as Kafka producers/consumers are deployed with the exact same way. Keep in mind you will have to change some values for a different Python script. Also you may need to add some modules using pip in the Dockerfile.
