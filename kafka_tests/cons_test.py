
#Simple tests for consuming

# Import KafkaConsumer from Kafka library
#from kafka import KafkaConsumer
#from json import loads
#import time

# Import sys module
#import sys

# Define server with port
#bootstrap_servers = ['kafka.default.svc.cluster.local:9092']

# Define topic name from where the message will recieve
#topicName = 'mytopic'

# Initialize consumer variable
#consumer = KafkaConsumer(topicName,bootstrap_servers= bootstrap_servers,value_deserializer=lambda x: loads(x.decode('utf-8')))

#Read and print message from consumer
#for msg in consumer: 

#    time.sleep(7)
#    print(msg)
#    print(msg.value)


#The following code section was used to test the sending of messages to Timescale DB.

# Import KafkaProducer from Kafka library
from kafka import KafkaProducer
from json import dumps
import time
import json
import re
import random

# Define server with port
bootstrap_servers = ['kafka-0.kafka-headless.default.svc.cluster.local:9092']

# Define topic name where the message will publish
topicName = 'throwaw1'

# Initialize producer variable
producer = KafkaProducer(bootstrap_servers = bootstrap_servers,value_serializer=lambda x: 
                         dumps(x).encode('utf-8'))

producer.flush()
#producer.send(topicName, b'Hello from kafka...')

counter=0
while True:
    time.sleep(5)
    counter += 1
    data = { "schema": {"type": "struct", "optional": False, "version": 1, "fields": [{ "field": "number", "type": "int32", "optional": True }]},"payload": {"number": counter}}
    #data2 = {'number' : counter*8}
    producer.send(topicName, value=data)
    #producer.send('sample2', value = temp_set)


print("Message Sent")
producer.close()


